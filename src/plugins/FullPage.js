

class FullPage{
    constructor() {
        this.currentPage = {
            id: 'first_page',
            index: 0
        };
        this.pages = document.getElementsByClassName('page');
        this.pagesInfo = [];
        this.isScrolling = false;
        this.touchOffsetY = 0;
        this.eventType = 'wheel'
        this.navElement = null;
    }

    setStartPage(page, i){
        if(page.offsetTop >= window.pageYOffset && (page.offsetTop - window.pageYOffset < 10)){
            this.currentPage = {
                id: page.id,
                index: i
            };
        }
    }



    getPagesInfo() {
        let dots = [], container = document.createElement('div');
        container.classList.add('containerDots');
        this.pagesInfo = Array.from(this.pages ? this.pages : document.getElementsByClassName('page')).map((page, i) => {
            dots[i] = document.createElement('div');
            dots[i].classList.add('dots');
            this.setStartPage(page, i);
            if (i === 0) {
                this.currentPage = {
                    id: page.id,
                    index: 0
                }
                dots[i].classList.add('active');
            }
            container.append(dots[i]);
            return {
                DOM: page,
                id: page.id,
                offsetTop: page.offsetTop
            }
        })

        document.body.appendChild(container);
        this.navElement = container;
    }


    scrollConditions(direction) {
        return !((this.currentPage.id === this.pagesInfo[0].id && direction === 'up')
            || ((this.currentPage.index + 1) === this.pagesInfo.length && direction !== 'up'));
    }

    scrollPage(e) {
        this.eventType = 'wheel';
        if (!this.isScrolling) {
            this.isScrolling = true;
            let offsetWindow = e.deltaY;

            if (offsetWindow < 0) {  // upscroll
                console.log('up scroll');
                this.getNextPage('up')
            } else {  //downscroll
                console.log('down scroll');
                this.getNextPage();
            }
        }else{
            e.preventDefault();
            return true;
        }
    }

    touchStart(e){
        this.touchOffsetY = e.touches[0].clientY;
        this.eventType = 'touch'
    }

    scrollPageTouch(e) {
        if (!this.isScrolling) {
            this.isScrolling = true;
            let offsetWindow = e.touches.length ? e.touches[0].clientY : e.changedTouches[0].clientY;

            if (offsetWindow > this.touchOffsetY) {  // upscroll
                console.log('up scroll');
                this.getNextPage('up')
            } else {  //downscroll
                console.log('down scroll');
                this.getNextPage();
            }
        }
    }

    getNextPage(direction = 'down') {
        if (this.scrollConditions(direction)) {
            let page, index;
            if (direction === 'up') {
                index = this.currentPage.index - 1
                page = this.pagesInfo[index];
            } else {
                index = this.currentPage.index + 1
                page = this.pagesInfo[index];
            }
            this.navElement.childNodes[this.currentPage.index].classList.remove('active');
            this.currentPage = {
                id: page.id,
                index: index
            }

            this.scrollTop(page);
        }

    }

    scrollTop(page) {
        if(this.eventType === 'wheel'){
            window.document.getElementById(page.id).scrollIntoView({ behavior: 'smooth'});
        }else{
            window.document.getElementById(page.id).scrollIntoView();
        }
        this.navElement.childNodes[this.currentPage.index].classList.add('active');
        /*page.DOM.scrollIntoView({ behavior: 'smooth', alignToTop : true});*/
        setTimeout(() => {
            this.isScrolling = false;
        }, 1000)
    }

}

export default FullPage