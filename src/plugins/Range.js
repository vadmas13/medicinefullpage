

class Range {
    constructor(rangeElement) {
        this.rangeElement = rangeElement;
        this.itemsLength = document.getElementsByClassName('slides__item').length;
        this.slideItems = Array.from(document.getElementsByClassName('slides__item')).map((item, i) => {
            return {
                DOM: item,
                index: i,
                value: Math.ceil(100 * i / (this.itemsLength - 1))
            }
        });
        this.slider = document.getElementById('slider');
        this.currentSlide = null;
        this.currentValue = 0;
        this.rangeElement.value = this.currentValue;
        this.maxValue = 100;
    }


    onChange(e) {
        this.transitionToCurrentValue()
    }

    setBackgroundProgress(value){
        this.rangeElement.style.background= `linear-gradient(to right, #fff,  
        #fff ${value}%, rgba(255, 255, 255, 0.42) ${value}%)`
    }

    onInput(e) {

        this.currentValue = parseInt(e.currentTarget.value);
        this.setBackgroundProgress(this.currentValue);
        this.slideItems.find((item, i) => {
            let lastItem = this.slideItems[i - 1] ? this.slideItems[i - 1] : 0;
            let nextItem = this.slideItems[i + 1] ? this.slideItems[i + 1] : this.maxValue;
            if (this.currentValue > item.value) {
                if (this.currentValue < nextItem.value) {
                    if ((nextItem.value - this.currentValue) > (this.currentValue - item.value)) {
                        this.currentValue = item.value;
                        this.currentSlide = item;
                        return true
                    } else {
                        this.currentValue = nextItem.value;
                        this.currentSlide = nextItem;
                        return true
                    }
                }else return false;
            } else {
                if ((item.value - this.currentValue) > (this.currentValue - lastItem.value)) {
                    this.currentValue = lastItem.value;
                    this.currentSlide = lastItem;
                    return true
                } else {
                    this.currentValue = item.value;
                    this.currentSlide = item;
                    return true
                }
            }
        })
        this.changeSlide();
    }

    transitionToCurrentValue(){
        let value = this.rangeElement.value;
        let nextValue = this.currentValue;

        let tr = setInterval(() => {
            if(nextValue > value){
                value++;
                this.rangeElement.value = value
            } else if(nextValue < value){
                value--;
                this.rangeElement.value = value
            }
            else if(nextValue === value) {
                clearInterval(tr)
            }
            this.setBackgroundProgress(value);
        }, 10)
    }

    changeSlide() {
        this.slider.style.transform = `translateX(-${this.currentSlide.index * 100}%)`;
    }

}

export default Range