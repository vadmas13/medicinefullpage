import React, {useEffect} from 'react';
import './App.scss';
import './assets/styles/styles.scss'
import FirstSection from "./components/Sections/FirstSection/FirstSection";
import SecondSection from "./components/Sections/SecondSection/SecondSection";
import ThirdSection from "./components/Sections/ThirdSection/ThirdSection";



function App({fullPagePlugin}) {


    useEffect(() => {
        fullPagePlugin.getPagesInfo();
    }, [ fullPagePlugin.pages.length ]) // eslint-disable-line


    return (
            <div id="myApp" >
                <section id="first_page" className="page" >
                    <FirstSection />
                </section>
                <section id="second_page" className="page">
                    <SecondSection />
                </section>
                <section id="third_page" className="page">
                    <ThirdSection />
                </section>
            </div>
        );

}

export default App;
