import React from "react";
import FullPage from "../plugins/FullPage";
import App from './../App';




const FullPageComponent = (props) => {


    const fullPagePlugin = new FullPage();

    const scrollBlocker = (e) => {
        if(!fullPagePlugin.isScrolling){
            return fullPagePlugin.scrollPage(e)
        }
    }


    window.addEventListener('wheel', scrollBlocker, false);
    // window.addEventListener('touchmove', fullPagePlugin.scrollPageTouch.bind(fullPagePlugin), true);
    window.addEventListener('touchstart', fullPagePlugin.touchStart.bind(fullPagePlugin), false);

    return (
        <App fullPagePlugin={fullPagePlugin}/>
    )
}
export default FullPageComponent;





