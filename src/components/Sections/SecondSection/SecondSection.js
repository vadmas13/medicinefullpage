import React from "react";
import Background from './../../../assets/images/bg/bg2.png'
import Iceberg from './../../../assets/images/bg/iceberg-copy.png'
import Crumbs from './../../../assets/images/bg/crumbs.png'
import styles from './SecondSection.module.scss'

const SecondSection = () => {
    return(
        <div className={styles.secondSection} style={{backgroundImage: `url(${Background})`}}>
            <h1 className={styles.secondSection__title}>Основа терапии —<br /> патогенез CD2</h1>
            <div className={styles.secondSection__icebegr} style={{backgroundImage: `url(${Iceberg})`}}></div>
            <div className={styles.secondSection__crumbs} style={{backgroundImage: `url(${Crumbs})`}}></div>
        </div>
    )
}

export default SecondSection