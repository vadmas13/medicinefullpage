import React, {createRef, useEffect} from "react";
import Background from './../../../assets/images/bg/bg1.jpg'
import Iceberg from './../../../assets/images/bg/iceberg-copy-2.png'
import styles from './firstSection.module.scss'
import IcebergMountaint from "../../../plugins/IcebergMountain";
import './areas.scss'

const FirstSection = () => {


    const ID = 'iceberg';
    const imageIceberg = createRef()

    useEffect(() => {
        new IcebergMountaint(imageIceberg.current);
    }, [imageIceberg])


    return (
        <div className={styles.firstSection} style={{
            backgroundImage: `url(${Background})`
        }}>
            <h1 className={styles.firstSection__title}>Всегда ли цели терапии CD2<br/> на поверхности?</h1>
            <div className={styles.firstSection__icebegr} id={ID}>
                <img src={Iceberg} className={styles.firstSection__img}
                     ref={imageIceberg}
                     alt="iceberg"/>
                <div className={`${styles.firstSection__area} ${styles.area}`} id='area1'>
                    <div className={styles.area__circle}></div>
                    <div className={`${styles.area__text} areaText`}>
                        Гипогликемия
                    </div>
                </div>
                <div className={`${styles.firstSection__area} ${styles.area}`} id='area2'>
                    <div className={styles.area__circle}></div>
                    <div className={`${styles.area__text} areaText`}>
                        Осложенения СД
                    </div>
                </div>
                <div className={`${styles.firstSection__area} ${styles.area}`} id='area3'>
                    <div className={styles.area__circle}></div>
                    <div className={`${styles.area__text} areaText`}>
                        Цель по HbA1c
                    </div>
                </div>
                <div className={`${styles.firstSection__area} ${styles.area}`} id='area4'>
                    <div className={styles.area__circle}></div>
                    <div className={`${styles.area__text} areaText`}>
                        CC риски
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FirstSection