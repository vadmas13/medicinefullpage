import React from "react";
import Background from './../../../assets/images/bg/bg3.png'
import styles from './ThirdSection.module.scss'
import RangeContainer from "../../Range/RangeContainer";

const ThirdSection = () => {
    return(
        <div className={styles.thirdSection} style={{backgroundImage: `url(${Background})`}}>
            <RangeContainer />
        </div>
    )
}

export default ThirdSection