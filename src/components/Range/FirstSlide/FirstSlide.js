import React from 'react'
import styles from './FirstSlide.module.scss'
import Betta from './../../../assets/images/betta.png'
import Capa from './../../../assets/images/capa.png'
import Musle from './../../../assets/images/musle.png'
import LongArrow from './../../../assets/images/arrows/long-arrow.png'
import smallArrow from './../../../assets/images/arrows/small-arrow.png'
import backgroungIce from './../../../assets/images/bg/slide1.png'

const FirstSlide = () => {
    return (
        <div className={styles.firstSlide} style={{backgroundImage: `url(${backgroungIce})`}}>
            <h2 className={styles.firstSlide__title}>Звенья патогенеза CD2</h2>
            <div className={styles.firstSlide__container}>
                <div className={`${styles.firstSlide__firstLvL} ${styles.LvL}`}>
                    <div className={styles.LvL__item}>
                        <div className={`${styles.LvL__arrows}  ${styles.arrows__long235}`}>
                            <img src={LongArrow} alt='arrow'/>
                        </div>
                        <div className={`${styles.LvL__arrows}  ${styles.arrows__long315}`}>
                            <img src={LongArrow} alt='arrow'/>
                        </div>
                        <div className={`${styles.LvL__arrows}  ${styles.arrows__small270}`}>
                            <img src={smallArrow} alt='arrow'/>
                        </div>
                        <div className={styles.LvL__steps}>
                            <p className={styles.LvL__number}>1</p>
                            <span className={styles.LvL__description}>β-клетки</span>
                        </div>
                        <img src={Betta} alt='betta' className={styles.LvL__icon}/>
                    </div>
                </div>
                <div className={styles.firstSlide__secondLvL}>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__small0}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__small180}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={styles.LvL__item} >
                        <div className={styles.LvL__steps}>
                            <p className={styles.LvL__number}>2</p>
                            <span className={styles.LvL__description}>Мышцы</span>
                        </div>
                        <img src={Musle} alt='Musle' className={styles.LvL__icon}/>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_text}`}>
                        <div className={styles.LvL__text}>
                            Гипергликемия
                        </div>
                    </div>
                    <div className={styles.LvL__item}>
                        <div className={styles.LvL__steps}>
                            <p className={styles.LvL__number}>3</p>
                            <span className={styles.LvL__description}>Печень</span>
                        </div>
                        <img src={Capa} alt='Capa' className={styles.LvL__icon}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FirstSlide