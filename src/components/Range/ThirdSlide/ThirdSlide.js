import React from 'react'
import styles from './ThirdSlide.module.scss'
import Betta from "../../../assets/images/betta.png";
import Lipid2016 from "../../../assets/images/lipid2016.png";
import Lipid from "../../../assets/images/lipid.png";
import Brain from "../../../assets/images/brain.png";
import Capa from "../../../assets/images/capa.png";
import Musle from "../../../assets/images/musle.png";
import Stomach from "../../../assets/images/stomach.png";
import Infection from "../../../assets/images/infection.png";
import BrainCopy from "../../../assets/images/brain1-copy.png";

import LongArrow from "../../../assets/images/arrows/long-arrow.png";
import smallArrow from "../../../assets/images/arrows/smallest-arrow.png";
import middleArrow from "../../../assets/images/arrows/middle-arrow2.png";
import middleArrow2 from "../../../assets/images/arrows/middle-arrow.png";
import backgroungIce from "../../../assets/images/bg/slide3.png";


const ThirdSlide = () => {
    return (
        <div className={styles.thirdSlide} style={{backgroundImage: `url(${backgroungIce})`}}>
            <h2 className={styles.thirdSlide__title}>Звенья патогенеза CD2</h2>
            <div className={styles.thirdSlide__container}>
                <div className={`${styles.thirdSlide__firstLvL} ${styles.LvL}`}>
                    <div className={styles.LvL__item}>
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_left}`}>
                            <p className={styles.LvL__number}>8</p>
                            <span className={styles.LvL__description}>Микрофлора кишечника</span>
                        </div>
                        <img src={BrainCopy} alt='BrainCopy' className={styles.LvL__icon}/>
                    </div>
                    <div className={styles.LvL__item}>
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_left}`}>
                            <p className={styles.LvL__number}>9</p>
                            <span className={styles.LvL__description}>Нарушение имунной регуляции/воспаление</span>
                        </div>
                        <img src={Infection} alt='Infection' className={styles.LvL__icon}/>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_text} ${styles.LvL__item_text_smallText}`}>
                        <span className={styles.LvL__item_fontWeight}>↓</span> амилин
                        <div className={`${styles.LvL__arrows} ${styles.arrows__middle340} ${styles.arrows__middle340_right} ${styles.arrows__middle340_transparentAmillin}`}>
                            <img src={middleArrow2} alt='middleArrow'/>
                        </div>
                        <div className={`${styles.LvL__arrows} ${styles.arrows__small270} ${styles.arrows__small270_transparent1} ${styles.arrows__small270_transparentAmilin}`}>
                            <img src={smallArrow} alt='smallArrow'/>
                        </div>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_translate11}`}>
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_left}`}>
                            <p className={styles.LvL__number}>10</p>
                            <span className={styles.LvL__description}>Желудок</span>
                        </div>
                        <img src={Stomach} alt='Stomach' className={styles.LvL__icon}/>
                        <div className={`${styles.LvL__arrows} ${styles.arrows__middle0} ${styles.arrows__middle0_transparentStomach}`}>
                            <img src={middleArrow2} alt='middleArrow'/>
                        </div>
                    </div>
                </div>
                <div className={styles.thirdSlide__secondLvL}>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__middle200}`}>
                        <img src={middleArrow2} alt='middleArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__middle200}  ${styles.arrows__middle200_right}`}>
                        <img src={middleArrow2} alt='middleArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__middle340} `}>
                        <img src={middleArrow2} alt='middleArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__middle340}  ${styles.arrows__middle340_right}`}>
                        <img src={middleArrow2} alt='middleArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__long225} `}>
                        <img src={middleArrow} alt='LongArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__long225}  ${styles.arrows__long225_right}`}>
                        <img src={middleArrow} alt='LongArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__long315} `}>
                        <img src={middleArrow2} alt='LongArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__long315}  ${styles.arrows__long315_right}`}>
                        <img src={middleArrow2} alt='LongArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__middle0} `}>
                        <img src={smallArrow} alt='middleArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__middle0}  ${styles.arrows__middle0_right}`}>
                        <img src={smallArrow} alt='middleArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__long0} `}>
                        <img src={LongArrow} alt='LongArrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__long330} `}>
                        <img src={LongArrow} alt='LongArrow'/>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_margin10} ${styles.LvL__item_betta}`} >
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps}`}>
                            <p className={styles.LvL__number}>1</p>
                            <span className={styles.LvL__description}>β-клетки</span>
                        </div>
                        <img src={Betta} alt='Betta' className={styles.LvL__icon}/>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_text} ${styles.LvL__item_marginNull}`}>
                        <div className={styles.LvL__inline}>
                            <div className={`${styles.LvL__steps} ${styles.LvL__steps_left} ${styles.LvL__steps_relative}`}>
                                <p className={styles.LvL__number}>2</p>
                                <span className={styles.LvL__description}>
                                    <span className={styles.LvL__item_fontWeight}>↓</span>
                                    инкретинового<br /> эффекта</span>
                            </div>
                            <div className={`${styles.LvL__steps} ${styles.LvL__steps_right} ${styles.LvL__steps_relative}`}>
                                <p className={styles.LvL__number}>3</p>
                                <span className={styles.LvL__description}>Дефект а-клеток</span>
                                <div className={`${styles.LvL__steps} ${styles.LvL__steps_down}`}>
                                    <span>
                                        <span className={styles.LvL__item_fontWeight}>↑</span>
                                        глюкагон
                                    </span>
                                    <div className={`${styles.LvL__arrows} ${styles.arrows__small270} `}>
                                        <img src={smallArrow} alt='smallArrow'/>
                                    </div>
                                    <div className={`${styles.LvL__arrows} ${styles.arrows__small270} ${styles.arrows__small270_down} `}>
                                        <img src={smallArrow} alt='smallArrow'/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_text}`}>
                        <div className={`${styles.LvL__text} ${styles.LvL__text_empty}`}>
                            <span className={`${styles.LvL__description} ${styles.LvL__description_margin0}`}>Гиперликемия</span>
                        </div>
                        <div className={`${styles.LvL__arrows} ${styles.arrows__small90} `}>
                            <img src={smallArrow} alt='smallArrow'/>
                        </div>
                        <div className={`${styles.LvL__arrows} ${styles.arrows__small270} ${styles.arrows__small270_transparent1}`}>
                            <img src={smallArrow} alt='smallArrow'/>
                        </div>
                    </div>
                    <div className={`${styles.LvL__item}`}>
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_left}`}>
                            <p className={styles.LvL__number}>11</p>
                            <span className={styles.LvL__description}>Почки</span>
                        </div>
                        <img src={Lipid2016} alt='Lipid2016' className={styles.LvL__icon}/>
                    </div>
                </div>
                <div className={styles.thirdSlide__thirdLvL}>
                    <div className={styles.LvL__item} >
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_right}`}>
                            <p className={styles.LvL__number}>7</p>
                            <span className={styles.LvL__description}>Мозг</span>
                        </div>
                        <img src={Brain} alt='Brain' className={styles.LvL__icon}/>
                        <div className={`${styles.LvL__arrows} ${styles.arrows__small90} ${styles.arrows__small90_transparentBrain} `}>
                            <img src={smallArrow} alt='smallArrow'/>
                        </div>
                        <div className={`${styles.LvL__arrows} ${styles.arrows__small270} ${styles.arrows__small270_transparent1} ${styles.arrows__small270_transparentBrain}`}>
                            <img src={smallArrow} alt='smallArrow'/>
                        </div>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_severalItems}`} >
                        <p className={styles.LvL__item_severalItems_title}>
                            Инсулинорезистентность
                        </p>
                        <div className={`${styles.LvL__item} ${styles.LvL__item_margin10}`} >
                            <div className={`${styles.LvL__steps} ${styles.LvL__steps_right}`}>
                                <p className={styles.LvL__number}>6</p>
                                <span className={styles.LvL__description}>Печнь</span>
                            </div>
                            <img src={Capa} alt='Capa' className={styles.LvL__icon}/>
                        </div>
                        <div className={`${styles.LvL__item} ${styles.LvL__item_margin10}`} >
                            <div className={`${styles.LvL__steps} ${styles.LvL__steps_right}`}>
                                <p className={styles.LvL__number}>5</p>
                                <span className={styles.LvL__description}>Мышцы</span>
                            </div>
                            <img src={Musle} alt='Musle' className={styles.LvL__icon}/>
                        </div>
                        <div className={`${styles.LvL__item} ${styles.LvL__item_margin10}`}>
                            <div className={`${styles.LvL__steps} ${styles.LvL__steps_right}`}>
                                <p className={styles.LvL__number}>4</p>
                                <span className={styles.LvL__description}>Жировые клетки</span>
                            </div>
                            <img src={Lipid} alt='Brain' className={styles.LvL__icon}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ThirdSlide