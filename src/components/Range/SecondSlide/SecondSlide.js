import React from 'react'
import styles from './SecondSlide.module.scss'
import smallArrow from "../../../assets/images/arrows/smallest-arrow.png";
import Betta from "../../../assets/images/betta.png";
import Musle from "../../../assets/images/musle.png";
import Capa from "../../../assets/images/capa.png";
import Brain from "../../../assets/images/brain.png";
import Lipid from "../../../assets/images/lipid.png";
import Lipid2016 from "../../../assets/images/lipid2016.png";
import backgroungIce from './../../../assets/images/bg/slide2.png'


const SecondSlide = () => {
    return (
        <div className={styles.secondSlide} style={{backgroundImage: `url(${backgroungIce})`}}>
            <h2 className={styles.secondSlide__title}>Смертельный октет</h2>
            <div className={styles.secondSlide__container}>
                <div className={`${styles.secondSlide__firstLvL} ${styles.LvL}`}>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__small270}  ${styles.arrows__small270_left}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__small270}  ${styles.arrows__small270_center}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__small270}  ${styles.arrows__small270_right}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={styles.LvL__item}>
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_left}`}>
                            <span className={styles.LvL__description}>Инкретиновый эффект</span>
                        </div>
                        <span className={styles.LvL__number}>2</span>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_betta}`}>
                        <div className={styles.LvL__steps}>
                            <p className={styles.LvL__number}>1</p>
                            <span className={styles.LvL__description}>β-клетки</span>
                        </div>
                        <img src={Betta} alt='betta' className={styles.LvL__icon}/>
                    </div>
                    <div className={styles.LvL__item}>
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_right}`}>
                            <span className={styles.LvL__description}>Дефект <br /> а-клеток</span>
                        </div>
                        <span className={styles.LvL__number}>3</span>
                    </div>
                </div>
                <div className={styles.secondSlide__secondLvL}>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__small0}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__small0}  ${styles.arrows__small0_right}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={styles.LvL__item} >
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_left}`}>
                            <p className={styles.LvL__number}>8</p>
                            <span className={styles.LvL__description}>Почки</span>
                        </div>
                        <img src={Lipid2016} alt='Lipid2016' className={styles.LvL__icon}/>
                    </div>
                    <div className={`${styles.LvL__item} ${styles.LvL__item_text}`}>
                        <div className={styles.LvL__text}>
                            Гипергликемия
                        </div>
                    </div>
                    <div className={styles.LvL__item}>
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_right}`}>
                            <p className={styles.LvL__number}>4</p>
                            <span className={styles.LvL__description}>Жировые <br/> клетки</span>
                        </div>
                        <img src={Lipid} alt='Lipid' className={styles.LvL__icon}/>
                    </div>
                </div>
                <div className={styles.secondSlide__thirdLvL}>
                    <div className={`${styles.LvL__arrows} ${styles.arrows__small90}  ${styles.arrows__small90_left}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__small90}  ${styles.arrows__small90_center}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={`${styles.LvL__arrows}  ${styles.arrows__small90}  ${styles.arrows__small90_right}`}>
                        <img src={smallArrow} alt='arrow'/>
                    </div>
                    <div className={styles.LvL__item} >
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_down}`}>
                            <p className={styles.LvL__number}>7</p>
                            <span className={styles.LvL__description}>Мозг</span>
                        </div>
                        <img src={Brain} alt='Brain' className={styles.LvL__icon}/>
                    </div>
                    <div className={styles.LvL__item} >
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_down}`}>
                            <p className={styles.LvL__number}>6</p>
                            <span className={styles.LvL__description}>Печень</span>
                        </div>
                        <img src={Capa} alt='Capa' className={styles.LvL__icon}/>
                    </div>
                    <div className={styles.LvL__item}>
                        <div className={`${styles.LvL__steps} ${styles.LvL__steps_down}`}>
                            <p className={styles.LvL__number}>5</p>
                            <span className={styles.LvL__description}>Мышцы</span>
                        </div>
                        <img src={Musle} alt='Musle' className={styles.LvL__icon}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SecondSlide