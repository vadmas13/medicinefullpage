import React, {useEffect} from 'react'
import '../../assets/styles/range.scss'
import FirstSlide from "./FirstSlide/FirstSlide";
import SecondSlide from "./SecondSlide/SecondSlide";
import ThirdSlide from "./ThirdSlide/ThirdSlide";
import Range from './../../plugins/Range'

const RangeContainer = () => {

    const rangeElement = React.createRef();

    useEffect(() => {
        const range = new Range(rangeElement.current);
        rangeElement.current.addEventListener('change', range.onChange.bind(range), false);
        rangeElement.current.addEventListener('input', range.onInput.bind(range), false);
    }, [rangeElement])

    return (
        <div className="container">
            <div className="slides" id="slider">
                <div className="slides__item">
                    <FirstSlide/>
                </div>
                <div className="slides__item">
                    <SecondSlide/>
                </div>
                <div className="slides__item">
                    <ThirdSlide/>
                </div>
            </div>
            <div className="container__rangeBlock rangeBlock">
                <input type="range" className="container__range slider" id="range" ref={rangeElement}/>
                <div className="rangeBlock__rangeData">
                    <div className="rangeBlock__item">1998</div>
                    <div className="rangeBlock__item">2009</div>
                    <div className="rangeBlock__item">2016</div>
                </div>
            </div>
        </div>
    )

}

export default RangeContainer;